# Text Summarization via NLP & Deep Learning
## Work in progress.

dataset comes from here:
https://www.kaggle.com/gunnvant/game-of-thrones-srt

The dataset contains every line from Game of Thrones (the show, not the books), grouped by episode. We're going to use NLP to summarise each episode - sort of like the text descriptions you get of episodes on Netflix.

