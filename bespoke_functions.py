import pandas as pd
import json

def json_to_df(season):
    
    "converts a json season into a dataframe, with one row per episode"
    
    # script is a dictionary of dictionaries - the keys are the names of the episodes
    episodes = [title for title in season.keys()]
    
    # create the dataframe by moving iteratively through the episodes in the season
    season_df = pd.DataFrame()
    
    for episode in episodes:
        line_numbers = season[episode].keys()
        lines = season[episode].values()
        episode_df = pd.DataFrame(index = [episode], columns = line_numbers, data = [lines])
        season_df = season_df.append(episode_df, sort = False)
    
    return season_df